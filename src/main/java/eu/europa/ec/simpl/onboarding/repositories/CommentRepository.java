package eu.europa.ec.simpl.onboarding.repositories;

import eu.europa.ec.simpl.onboarding.entities.Comment;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CommentRepository extends JpaRepository<Comment, UUID> {

    List<Comment> findByOnboardingRequestIdIn(Set<UUID> onboardingRequestIds);
}
