package eu.europa.ec.simpl.onboarding.configurations;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "microservice")
public record MicroserviceProperties(UsersRoles usersRoles, IdentityProvider identityProvider) {

    public record UsersRoles(String url) {}

    public record IdentityProvider(String url) {}
}
