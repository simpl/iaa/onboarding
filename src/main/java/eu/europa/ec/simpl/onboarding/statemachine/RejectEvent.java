package eu.europa.ec.simpl.onboarding.statemachine;

import eu.europa.ec.simpl.common.model.dto.onboarding.RejectDTO;
import eu.europa.ec.simpl.onboarding.statemachine.common.BasicEvent;

public interface RejectEvent extends BasicEvent<OnboardingState, OnboardingPayload<RejectDTO>> {

    @Override
    default OnboardingState getSource() {
        return OnboardingState.IN_REVIEW;
    }

    @Override
    default OnboardingState getTarget() {
        return OnboardingState.REJECTED;
    }
}
