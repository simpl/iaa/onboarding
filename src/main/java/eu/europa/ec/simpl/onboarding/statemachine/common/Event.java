package eu.europa.ec.simpl.onboarding.statemachine.common;

import java.util.Set;

public interface Event<S extends State, P> {

    void handle(S source, S target, P payload);

    default boolean guard(S source, S target, P payload) {
        return true;
    }

    Set<Edge<S>> getEdges();

    interface Edge<S> {
        S getSource();

        S getTarget();

        static <S> Edge<S> of(S source, S target) {
            return new Edge<>() {
                @Override
                public S getSource() {
                    return source;
                }

                @Override
                public S getTarget() {
                    return target;
                }
            };
        }
    }
}
