package eu.europa.ec.simpl.onboarding.controllers;

import eu.europa.ec.simpl.api.onboarding.v1.exchanges.ParticipantTypesApi;
import eu.europa.ec.simpl.api.onboarding.v1.model.ParticipantTypeDTO;
import eu.europa.ec.simpl.onboarding.mappers.ParticipantTypeMapperV1;
import java.util.List;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("v1")
public class ParticipantTypeControllerV1 implements ParticipantTypesApi {

    private final ParticipantTypeController controller;
    private final ParticipantTypeMapperV1 mapper;

    public ParticipantTypeControllerV1(ParticipantTypeController controller, ParticipantTypeMapperV1 mapper) {
        this.controller = controller;
        this.mapper = mapper;
    }

    @Override
    public List<ParticipantTypeDTO> getParticipantTypes() {
        return mapper.toV1(controller.getParticipantTypes());
    }
}
