package eu.europa.ec.simpl.onboarding.repositories;

import eu.europa.ec.simpl.onboarding.entities.MimeType;
import java.util.Optional;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MimeTypeRepository extends JpaRepository<MimeType, UUID> {
    Optional<MimeType> findMimeTypeByValue(String value);
}
