package eu.europa.ec.simpl.onboarding.enums;

public enum ViolationType {
    DOCUMENT_NOT_REQUIRED,
    CONTENT_DOES_NOT_MATCH_MIME_TYPE;
}
