package eu.europa.ec.simpl.onboarding.exceptions;

import eu.europa.ec.simpl.common.exceptions.StatusException;
import java.util.UUID;
import org.springframework.http.HttpStatus;

public class ParticipantTypeNotFoundException extends StatusException {
    public ParticipantTypeNotFoundException(UUID pTypeId) {
        this(HttpStatus.NOT_FOUND, pTypeId);
    }

    public ParticipantTypeNotFoundException(String pType) {
        this(HttpStatus.NOT_FOUND, pType);
    }

    public ParticipantTypeNotFoundException(HttpStatus status, UUID pTypeId) {
        super(status, "Participant type with id [ %s ] not found".formatted(pTypeId));
    }

    public ParticipantTypeNotFoundException(HttpStatus status, String pTypeId) {
        super(status, "Participant type [ %s ] not found".formatted(pTypeId));
    }
}
