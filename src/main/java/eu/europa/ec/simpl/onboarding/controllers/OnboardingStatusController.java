package eu.europa.ec.simpl.onboarding.controllers;

import eu.europa.ec.simpl.common.model.dto.onboarding.OnboardingStatusDTO;
import eu.europa.ec.simpl.onboarding.services.OnboardingStatusService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import jakarta.validation.constraints.NotBlank;
import java.util.List;
import java.util.UUID;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("onboarding-status")
public class OnboardingStatusController {

    private final OnboardingStatusService onboardingStatusService;

    public OnboardingStatusController(OnboardingStatusService onboardingStatusService) {
        this.onboardingStatusService = onboardingStatusService;
    }

    @Operation(
            summary = "Get Onboarding Status",
            description = "Retrieves the onboarding status",
            responses = {
                @ApiResponse(
                        responseCode = "200",
                        description = "onboarding status retrieved successfully",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        array =
                                                @ArraySchema(
                                                        schema = @Schema(implementation = OnboardingStatusDTO.class)))),
                @ApiResponse(responseCode = "401", description = "Access denied"),
                @ApiResponse(responseCode = "403", description = "Forbidden: User does not have the required role"),
            })
    @GetMapping
    public List<OnboardingStatusDTO> getStatus() {
        return onboardingStatusService.getOnboardingStatus();
    }

    @Operation(
            summary = "Set Onboarding Status Label",
            description = "Set the onboarding status label",
            responses = {
                @ApiResponse(responseCode = "204", description = "onboarding status label set correctly"),
                @ApiResponse(responseCode = "401", description = "Access denied"),
                @ApiResponse(responseCode = "403", description = "Forbidden: User does not have the required role"),
                @ApiResponse(responseCode = "404", description = "Not found: No Status associated to the id"),
            })
    @PatchMapping("{id}")
    public void setLabel(@PathVariable UUID id, @RequestParam @NotBlank String label) {
        onboardingStatusService.setOnboardingStatusLabel(id, label);
    }
}
