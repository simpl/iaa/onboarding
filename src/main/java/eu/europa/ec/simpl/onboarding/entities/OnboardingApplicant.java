package eu.europa.ec.simpl.onboarding.entities;

import eu.europa.ec.simpl.common.entity.annotations.UUIDv7Generator;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import java.util.UUID;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.hibernate.annotations.ColumnTransformer;

@Accessors(chain = true)
@ToString
@Getter
@Setter
@Entity
@Table(name = "onboarding_applicant")
@NoArgsConstructor
public class OnboardingApplicant {

    @Id
    @UUIDv7Generator
    private UUID id;

    @Column(name = "username", nullable = false)
    private String username;

    @Column(name = "firstname", nullable = false)
    private String firstName;

    @Column(name = "lastname", nullable = false)
    private String lastName;

    @ColumnTransformer(read = "lower(email)", write = "lower(?)")
    @Column(name = "email", nullable = false)
    private String email;
}
