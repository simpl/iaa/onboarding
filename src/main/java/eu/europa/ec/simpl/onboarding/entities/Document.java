package eu.europa.ec.simpl.onboarding.entities;

import eu.europa.ec.simpl.common.entity.annotations.UUIDv7Generator;
import eu.europa.ec.simpl.common.model.interfaces.DocumentModel;
import jakarta.persistence.Basic;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.Lob;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import java.sql.Blob;
import java.sql.Types;
import java.time.Instant;
import java.util.Optional;
import java.util.UUID;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.annotations.UpdateTimestamp;

@Accessors(chain = true)
@ToString
@Getter
@Setter
@Entity
@Table(name = "document")
@NoArgsConstructor
public class Document implements DocumentModel {
    @Id
    @UUIDv7Generator
    private UUID id;

    @ToString.Exclude
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "document_template_id")
    private DocumentTemplate documentTemplate;

    @ToString.Exclude
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "onboarding_request_id", nullable = false)
    private OnboardingRequest onboardingRequest;

    @ToString.Exclude
    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(name = "mime_type_id", nullable = true)
    private MimeType mimeType;

    @Column(name = "description")
    private String description;

    @Column(name = "filename")
    private String filename;

    @Column(name = "filesize")
    private Long filesize;

    @Lob
    @ToString.Exclude
    @Column(name = "content")
    @JdbcTypeCode(Types.BINARY)
    @Basic(fetch = FetchType.LAZY)
    private Blob content;

    @CreationTimestamp
    @Column(name = "creation_timestamp", nullable = false)
    private Instant creationTimestamp;

    @UpdateTimestamp
    @Column(name = "update_timestamp", nullable = false)
    private Instant updateTimestamp;

    public MimeType getActualMimeType() {
        return Optional.ofNullable(getMimeType())
                .orElseGet(() -> getDocumentTemplate().getMimeType());
    }

    public Boolean isMandatory() {
        return documentTemplate == null || documentTemplate.isMandatory();
    }
}
