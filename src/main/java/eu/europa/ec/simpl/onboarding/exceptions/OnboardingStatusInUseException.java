package eu.europa.ec.simpl.onboarding.exceptions;

import eu.europa.ec.simpl.common.exceptions.StatusException;
import org.springframework.http.HttpStatus;

public class OnboardingStatusInUseException extends StatusException {

    public OnboardingStatusInUseException(String value) {
        super(
                HttpStatus.FORBIDDEN,
                "The onboarding status [%s] is already in use and cannot be deleted".formatted(value));
    }
}
