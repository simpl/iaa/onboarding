package eu.europa.ec.simpl.onboarding.services.impl;

import eu.europa.ec.simpl.common.model.dto.onboarding.CommentDTO;
import eu.europa.ec.simpl.onboarding.mappers.CommentMapper;
import eu.europa.ec.simpl.onboarding.repositories.CommentRepository;
import eu.europa.ec.simpl.onboarding.services.CommentService;
import java.util.UUID;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

@Log4j2
@Service
@Validated
public class CommentServiceImpl implements CommentService {

    private final CommentRepository commentRepository;
    private final CommentMapper commentMapper;

    public CommentServiceImpl(CommentRepository commentRepository, CommentMapper commentMapper) {
        this.commentRepository = commentRepository;
        this.commentMapper = commentMapper;
    }

    @Override
    public CommentDTO createComment(UUID onboardingRequestId, CommentDTO commentDTO) {
        return commentMapper.toCommentDto(
                commentRepository.saveAndFlush(commentMapper.toEntity(commentDTO, onboardingRequestId)));
    }
}
