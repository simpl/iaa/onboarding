package eu.europa.ec.simpl.onboarding.services;

import eu.europa.ec.simpl.common.model.dto.onboarding.OnboardingStatusDTO;
import eu.europa.ec.simpl.common.model.dto.onboarding.OnboardingStatusValue;
import java.util.List;
import java.util.UUID;

public interface OnboardingStatusService {

    List<OnboardingStatusDTO> getOnboardingStatus();

    void setOnboardingStatusLabel(UUID id, String label);

    OnboardingStatusDTO findOnboardingStatusByStatus(OnboardingStatusValue onboardingStatusEnum);
}
