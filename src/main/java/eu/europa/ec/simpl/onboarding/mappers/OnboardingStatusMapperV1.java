package eu.europa.ec.simpl.onboarding.mappers;

import eu.europa.ec.simpl.api.onboarding.v1.model.OnboardingStatusDTO;
import java.util.List;
import lombok.Generated;
import org.mapstruct.AnnotateWith;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(unmappedSourcePolicy = ReportingPolicy.ERROR, unmappedTargetPolicy = ReportingPolicy.ERROR)
@AnnotateWith(Generated.class)
public interface OnboardingStatusMapperV1 {

    List<OnboardingStatusDTO> toV1(List<eu.europa.ec.simpl.common.model.dto.onboarding.OnboardingStatusDTO> status);
}
