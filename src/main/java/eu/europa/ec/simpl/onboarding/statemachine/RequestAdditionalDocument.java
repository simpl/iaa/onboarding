package eu.europa.ec.simpl.onboarding.statemachine;

import eu.europa.ec.simpl.common.model.dto.onboarding.DocumentDTO;
import eu.europa.ec.simpl.onboarding.statemachine.common.BasicEvent;

public interface RequestAdditionalDocument extends BasicEvent<OnboardingState, OnboardingPayload<DocumentDTO>> {

    @Override
    default OnboardingState getSource() {
        return OnboardingState.IN_REVIEW;
    }

    @Override
    default OnboardingState getTarget() {
        return OnboardingState.IN_REVIEW;
    }
}
