package eu.europa.ec.simpl.onboarding.liquibase;

import java.sql.Connection;
import org.apache.commons.dbutils.QueryRunner;

public class QueryFactory {

    private final Connection connection;

    public QueryFactory(Connection connection) {
        this.connection = connection;
    }

    public <T> Query<T> createQuery(Class<T> clazz, String query, Object... params) {
        return new Query<>(connection, new QueryRunner(), clazz, query, params);
    }
}
