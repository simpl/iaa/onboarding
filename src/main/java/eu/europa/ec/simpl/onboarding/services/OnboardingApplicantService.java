package eu.europa.ec.simpl.onboarding.services;

import eu.europa.ec.simpl.common.model.dto.onboarding.OnboardingApplicantDTO;
import eu.europa.ec.simpl.common.model.dto.onboarding.ParticipantTypeDTO;
import eu.europa.ec.simpl.onboarding.entities.OnboardingApplicant;
import jakarta.validation.constraints.NotNull;
import java.util.List;

public interface OnboardingApplicantService {

    OnboardingApplicant create(
            @NotNull OnboardingApplicantDTO applicantDTO,
            @NotNull ParticipantTypeDTO participantType,
            @NotNull String organization);

    List<OnboardingApplicantDTO> findOnboardingApplicant(String email);
}
