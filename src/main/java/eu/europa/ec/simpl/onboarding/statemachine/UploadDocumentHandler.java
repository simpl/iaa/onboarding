package eu.europa.ec.simpl.onboarding.statemachine;

import eu.europa.ec.simpl.common.model.dto.onboarding.DocumentDTO;
import eu.europa.ec.simpl.common.security.JwtService;
import eu.europa.ec.simpl.onboarding.enums.ViolationType;
import eu.europa.ec.simpl.onboarding.exceptions.InvalidDocumentException;
import eu.europa.ec.simpl.onboarding.services.DocumentService;
import eu.europa.ec.simpl.onboarding.utils.MimeTypeUtil;
import org.springframework.stereotype.Component;

@Component
public class UploadDocumentHandler implements UploadDocument, ApplicantEvent<DocumentDTO> {

    private final JwtService jwtService;
    private final DocumentService documentService;

    public UploadDocumentHandler(JwtService jwtService, DocumentService documentService) {
        this.jwtService = jwtService;
        this.documentService = documentService;
    }

    @Override
    public void handle(OnboardingState source, OnboardingState target, OnboardingPayload<DocumentDTO> payload) {
        var onboardingRequest = payload.getOnboardingRequest();
        var document = payload.getActionPayload();

        var requiredDocument = onboardingRequest.getDocumentById(document.getId());

        if (requiredDocument.isEmpty()) {
            throw new InvalidDocumentException(document.getId(), ViolationType.DOCUMENT_NOT_REQUIRED);
        }

        var expectedMimeType = requiredDocument.get().getActualMimeType().getValue();

        var matchesMimeType = MimeTypeUtil.hasMatchingMimeType(expectedMimeType, document.getContent());

        if (!matchesMimeType) {
            throw new InvalidDocumentException(document.getId(), ViolationType.CONTENT_DOES_NOT_MATCH_MIME_TYPE);
        }

        documentService.uploadDocument(document);
    }

    @Override
    public boolean guard(OnboardingState source, OnboardingState target, OnboardingPayload<DocumentDTO> payload) {
        return ApplicantEvent.super.guard(source, target, payload);
    }

    @Override
    public JwtService getJwtService() {
        return jwtService;
    }
}
