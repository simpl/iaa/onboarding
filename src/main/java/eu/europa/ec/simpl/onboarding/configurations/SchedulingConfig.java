package eu.europa.ec.simpl.onboarding.configurations;

import eu.europa.ec.simpl.onboarding.scheduled.RejectionOfStaleOnboardingRequestsTask;
import javax.sql.DataSource;
import net.javacrumbs.shedlock.core.LockProvider;
import net.javacrumbs.shedlock.provider.jdbctemplate.JdbcTemplateLockProvider;
import net.javacrumbs.shedlock.spring.annotation.EnableSchedulerLock;
import net.javacrumbs.shedlock.spring.annotation.SchedulerLock;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@Configuration
@EnableScheduling
@EnableSchedulerLock(defaultLockAtMostFor = "10m")
public class SchedulingConfig {

    private final RejectionOfStaleOnboardingRequestsTask rejectionOfStaleOnboardingRequestsTask;

    public SchedulingConfig(RejectionOfStaleOnboardingRequestsTask rejectionOfStaleOnboardingRequestsTask) {
        this.rejectionOfStaleOnboardingRequestsTask = rejectionOfStaleOnboardingRequestsTask;
    }

    @Bean
    public LockProvider lockProvider(DataSource dataSource) {
        return new JdbcTemplateLockProvider(JdbcTemplateLockProvider.Configuration.builder()
                .withJdbcTemplate(new JdbcTemplate(dataSource))
                .usingDbTime()
                .build());
    }

    @Scheduled(cron = "${scheduledTask.rejectionOfStaleOnboardingRequests.cron}")
    @SchedulerLock(name = "rejectionOfStaleOnboardingRequests")
    public void rejectionOfStaleOnboardingRequestsTask() {
        this.rejectionOfStaleOnboardingRequestsTask.rejectionOfStaleOnboardingRequestsTask();
    }
}
