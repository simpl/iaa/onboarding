package eu.europa.ec.simpl.onboarding.liquibase.migration_2024_11_25;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.europa.ec.simpl.common.model.dto.usersroles.KeycloakUserDTO;
import eu.europa.ec.simpl.onboarding.liquibase.QueryExecutor;
import eu.europa.ec.simpl.onboarding.liquibase.QueryFactory;
import eu.europa.ec.simpl.onboarding.liquibase.migration_2024_11_25.pojo.CredentialRequest;
import eu.europa.ec.simpl.onboarding.liquibase.migration_2024_11_25.pojo.OnboardingApplicant;
import eu.europa.ec.simpl.onboarding.liquibase.migration_2024_11_25.pojo.OnboardingRequest;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;
import liquibase.change.custom.CustomTaskChange;
import liquibase.database.Database;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.CustomChangeException;
import liquibase.exception.DatabaseException;
import liquibase.exception.SetupException;
import liquibase.exception.ValidationErrors;
import liquibase.resource.ResourceAccessor;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.MediaType;

@Log4j2
public class OnboardingApplicantMigrator implements CustomTaskChange {

    @Override
    public void execute(Database database) throws CustomChangeException {
        try {
            process(database);
        } catch (SQLException | DatabaseException e) {
            throw new RuntimeException(e);
        }
    }

    private void process(Database database) throws SQLException, DatabaseException {

        JdbcConnection connection = getConnection(database);
        var factory = newQueryFactory(connection);
        var onboardingRequestRepo = newQueryExecutor(factory, OnboardingRequest.class);
        var credentialRequestRepo = newQueryExecutor(factory, CredentialRequest.class);
        var onboardingApplicantRepo = newQueryExecutor(factory, OnboardingApplicant.class);

        var existingOnboardingRequests = onboardingRequestRepo.queryList(
                "select id, applicant_representative as applicantRepresentative from onboarding_request");

        var kcUsers = existingOnboardingRequests.stream()
                .map(e -> Map.entry(e.getApplicantRepresentative(), getUserByEmail(e.getApplicantRepresentative())))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

        var credentialRequests = credentialRequestRepo
                .queryList(
                        "select applicant_representative as applicantRepresentative, participant_type_id as participantTypeId, organization from credential_request")
                .stream()
                .collect(Collectors.toMap(CredentialRequest::getApplicantRepresentative, Function.identity()));

        existingOnboardingRequests.stream()
                .map(or -> {
                    var email = or.getApplicantRepresentative();
                    var user = kcUsers.get(email);
                    var request = credentialRequests.get(email);
                    var applicantId = UUID.randomUUID();

                    onboardingRequestRepo.insert(
                            "update onboarding_request set onboarding_applicant_id=?, organization=?, participant_type_id=? where id=? ",
                            applicantId,
                            request.getOrganization(),
                            request.getParticipantTypeId(),
                            or.getId());

                    var username = user.map(KeycloakUserDTO::getUsername).orElse("unknown");
                    var firstname = user.map(KeycloakUserDTO::getFirstName).orElse("unknown");
                    var lastname = user.map(KeycloakUserDTO::getLastName).orElse("unknown");

                    return new OnboardingApplicant(applicantId, username, firstname, lastname, email);
                })
                .forEach(oa -> onboardingApplicantRepo.insert(
                        "insert into onboarding_applicant values (?,?,?,?,?)",
                        oa.getId(),
                        oa.getUsername(),
                        oa.getFirstName(),
                        oa.getLastName(),
                        oa.getEmail()));
    }

    @SneakyThrows
    private Optional<KeycloakUserDTO> getUserByEmail(String email) {
        var usersRolesUrl = getenv("MICROSERVICE_USERS_ROLES_URL");
        log.debug("getting user from url: {}", usersRolesUrl);
        var response = HttpClient.newHttpClient()
                .send(
                        HttpRequest.newBuilder(
                                        URI.create("%s/user/search?email=%s&max=1".formatted(usersRolesUrl, email)))
                                .GET()
                                .headers(
                                        "Content-Type",
                                        MediaType.APPLICATION_JSON_VALUE,
                                        "Accept",
                                        MediaType.APPLICATION_JSON_VALUE)
                                .build(),
                        HttpResponse.BodyHandlers.ofString())
                .body();
        var out = new ObjectMapper().readValue(response, new TypeReference<List<KeycloakUserDTO>>() {});
        if (out.isEmpty()) {
            log.warn("no user found on keycloak with email {}", email);
            return Optional.empty();
        }
        return Optional.of(out.getFirst());
    }

    @Override
    public String getConfirmationMessage() {
        return "";
    }

    @Override
    public void setUp() throws SetupException {}

    @Override
    public void setFileOpener(ResourceAccessor resourceAccessor) {}

    @Override
    public ValidationErrors validate(Database database) {
        return null;
    }

    protected <T> QueryExecutor<T> newQueryExecutor(QueryFactory factory, Class<T> clazz) {
        return new QueryExecutor<>(factory, clazz);
    }

    protected JdbcConnection getConnection(Database database) {
        return (JdbcConnection) database.getConnection();
    }

    protected QueryFactory newQueryFactory(JdbcConnection connection) {
        return new QueryFactory(connection.getUnderlyingConnection());
    }

    protected String getenv(String key) {
        return System.getenv(key);
    }
}
