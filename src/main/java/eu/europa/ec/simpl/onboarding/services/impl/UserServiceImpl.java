package eu.europa.ec.simpl.onboarding.services.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.europa.ec.simpl.common.exchanges.usersroles.UserExchange;
import eu.europa.ec.simpl.common.model.dto.usersroles.KeycloakUserDTO;
import eu.europa.ec.simpl.onboarding.exceptions.UserCreationException;
import eu.europa.ec.simpl.onboarding.services.UserService;
import java.util.Optional;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ProblemDetail;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.client.HttpClientErrorException;

@Log4j2
@Service
@Validated
public class UserServiceImpl implements UserService {

    private final UserExchange userExchange;

    public UserServiceImpl(UserExchange userExchange) {
        this.userExchange = userExchange;
    }

    @Override
    public String createUser(KeycloakUserDTO keycloakUserDTO) {
        try {
            return userExchange.createUser(keycloakUserDTO);
        } catch (HttpClientErrorException e) {
            var errorBody = e.getResponseBodyAsString();
            var error = Optional.ofNullable(parseError(errorBody))
                    .map(ProblemDetail::getDetail)
                    .orElse("Unknown error");
            throw new UserCreationException(HttpStatus.valueOf(e.getStatusCode().value()), error);
        }
    }

    private static ProblemDetail parseError(String errorBody) {
        try {
            return new ObjectMapper().readValue(errorBody, ProblemDetail.class);
        } catch (JsonProcessingException ignored) {
            return null;
        }
    }
}
