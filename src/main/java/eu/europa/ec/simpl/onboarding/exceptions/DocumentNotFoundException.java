package eu.europa.ec.simpl.onboarding.exceptions;

import eu.europa.ec.simpl.common.exceptions.StatusException;
import java.util.UUID;
import org.springframework.http.HttpStatus;

public class DocumentNotFoundException extends StatusException {
    public DocumentNotFoundException(UUID id) {
        this(HttpStatus.NOT_FOUND, id);
    }

    public DocumentNotFoundException(HttpStatus httpStatus, UUID id) {
        super(httpStatus, "Document with id %s not found".formatted(id));
    }

    public DocumentNotFoundException(String message) {
        super(HttpStatus.NOT_FOUND, message);
    }
}
