package eu.europa.ec.simpl.onboarding.services.impl;

import eu.europa.ec.simpl.common.model.dto.onboarding.OnboardingStatusDTO;
import eu.europa.ec.simpl.common.model.dto.onboarding.OnboardingStatusValue;
import eu.europa.ec.simpl.onboarding.exceptions.OnboardingStatusNotFoundException;
import eu.europa.ec.simpl.onboarding.mappers.OnboardingStatusMapper;
import eu.europa.ec.simpl.onboarding.repositories.OnboardingStatusRepository;
import eu.europa.ec.simpl.onboarding.services.OnboardingStatusService;
import java.util.List;
import java.util.UUID;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

@Log4j2
@Service
@Validated
public class OnboardingStatusServiceImpl implements OnboardingStatusService {

    private final OnboardingStatusRepository statusRepository;
    private final OnboardingStatusMapper mapper;

    public OnboardingStatusServiceImpl(OnboardingStatusRepository statusRepository, OnboardingStatusMapper mapper) {
        this.statusRepository = statusRepository;
        this.mapper = mapper;
    }

    @Override
    public List<OnboardingStatusDTO> getOnboardingStatus() {
        return statusRepository.findAll().stream().map(mapper::toDto).toList();
    }

    @Transactional
    @Override
    public void setOnboardingStatusLabel(UUID id, String label) {
        var onboardingStatus = statusRepository.findById(id).orElseThrow(OnboardingStatusNotFoundException::new);
        onboardingStatus.setLabel(label);
    }

    @Override
    public OnboardingStatusDTO findOnboardingStatusByStatus(OnboardingStatusValue onboardingStatusEnum) {
        return statusRepository
                .findByValue(onboardingStatusEnum)
                .map(mapper::toDto)
                .orElseThrow(OnboardingStatusNotFoundException::new);
    }
}
