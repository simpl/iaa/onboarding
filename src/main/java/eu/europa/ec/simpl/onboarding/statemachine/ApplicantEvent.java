package eu.europa.ec.simpl.onboarding.statemachine;

import eu.europa.ec.simpl.common.constants.Roles;
import eu.europa.ec.simpl.common.security.JwtService;
import eu.europa.ec.simpl.onboarding.exceptions.CannotEditOnboardingRequestException;
import eu.europa.ec.simpl.onboarding.exceptions.OperationNotPermittedException;

interface ApplicantEvent<T> {

    default boolean guard(OnboardingState source, OnboardingState target, OnboardingPayload<T> payload) {
        if (!getJwtService().hasRole(Roles.APPLICANT)) {
            throw new OperationNotPermittedException();
        }
        if (payload.getOnboardingRequest()
                .getOnboardingApplicant()
                .getEmail()
                .equals(getJwtService().getEmail())) {
            return true;
        } else {
            throw new CannotEditOnboardingRequestException(
                    getJwtService().getEmail(), payload.getOnboardingRequest().getId());
        }
    }

    JwtService getJwtService();
}
