package eu.europa.ec.simpl.onboarding.controllers;

import eu.europa.ec.simpl.api.onboarding.v1.exchanges.MimeTypesApi;
import eu.europa.ec.simpl.api.onboarding.v1.model.MimeTypeDTO;
import eu.europa.ec.simpl.api.onboarding.v1.model.PagedModelMimeTypeDTO;
import eu.europa.ec.simpl.onboarding.mappers.MimeTypeMapperV1;
import java.util.List;
import java.util.UUID;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("v1")
public class MimeTypeControllerV1 implements MimeTypesApi {

    private final MimeTypeController controller;
    private final MimeTypeMapperV1 mapper;

    public MimeTypeControllerV1(MimeTypeController controller, MimeTypeMapperV1 mapper) {
        this.controller = controller;
        this.mapper = mapper;
    }

    @Override
    public UUID createMimeType(MimeTypeDTO mimeTypeDTO) {
        return controller.createMimeType(mapper.toV0(mimeTypeDTO));
    }

    @Override
    public void deleteMimeType(UUID id) {
        controller.deleteMimeType(id);
    }

    @Override
    public List<String> getAllAvailableMimeTypes(String search) {
        return controller.getAllAvailableMimeTypes(search);
    }

    @Override
    public PagedModelMimeTypeDTO getAllMimeTypes(Integer page, Integer size, List<String> sort, Object filter) {
        return mapper.toV1(
                controller.getAllMimeTypes(PageRequest.of(page, size, Sort.by(sort.toArray(String[]::new)))));
    }

    @Override
    public MimeTypeDTO getMimeType(UUID id) {
        return mapper.toV1(controller.getMimeType(id));
    }

    @Override
    public void updateMimeType(UUID id, MimeTypeDTO mimeTypeDTO) {
        controller.updateMimeType(id, mapper.toV0(mimeTypeDTO));
    }
}
