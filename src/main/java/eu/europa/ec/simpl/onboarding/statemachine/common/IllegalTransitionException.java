package eu.europa.ec.simpl.onboarding.statemachine.common;

import lombok.Getter;

@Getter
public class IllegalTransitionException extends RuntimeException {

    private final Object state;
    private final Object event;

    public IllegalTransitionException(String state, String event) {
        super("No transition allowed from state %s with event %s".formatted(state, event));
        this.state = state;
        this.event = event;
    }
}
