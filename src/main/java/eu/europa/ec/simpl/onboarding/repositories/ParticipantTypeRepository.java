package eu.europa.ec.simpl.onboarding.repositories;

import eu.europa.ec.simpl.onboarding.entities.ParticipantType;
import eu.europa.ec.simpl.onboarding.exceptions.ParticipantTypeNotFoundException;
import java.util.Optional;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ParticipantTypeRepository extends JpaRepository<ParticipantType, UUID> {

    Optional<ParticipantType> findByValueIgnoreCase(String value);

    default ParticipantType findByValueIgnoreCaseOrThrow(String value) {
        return findByValueIgnoreCase(value).orElseThrow(() -> new ParticipantTypeNotFoundException(value));
    }
}
