package eu.europa.ec.simpl.onboarding.mappers;

import eu.europa.ec.simpl.api.onboarding.v1.model.DocumentDTO;
import eu.europa.ec.simpl.api.onboarding.v1.model.OnboardingRequestDTO;
import eu.europa.ec.simpl.api.onboarding.v1.model.PageResponseOnboardingRequestDTO;
import eu.europa.ec.simpl.api.onboarding.v1.model.SearchFilterParameterDTO;
import eu.europa.ec.simpl.common.filters.OnboardingRequestFilter;
import eu.europa.ec.simpl.common.model.dto.onboarding.ApproveDTO;
import eu.europa.ec.simpl.common.model.dto.onboarding.CommentDTO;
import eu.europa.ec.simpl.common.model.dto.onboarding.RejectDTO;
import eu.europa.ec.simpl.common.responses.PageResponse;
import java.util.Base64;
import lombok.Generated;
import org.mapstruct.AnnotateWith;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

@Mapper(unmappedSourcePolicy = ReportingPolicy.ERROR, unmappedTargetPolicy = ReportingPolicy.ERROR)
@AnnotateWith(Generated.class)
public interface OnboardingRequestMapperV1 {
    CommentDTO toV0(eu.europa.ec.simpl.api.onboarding.v1.model.CommentDTO commentDTO);

    OnboardingRequestDTO toV1(eu.europa.ec.simpl.common.model.dto.onboarding.OnboardingRequestDTO onboardingRequestDTO);

    ApproveDTO toV0(eu.europa.ec.simpl.api.onboarding.v1.model.ApproveDTO approveDTO);

    eu.europa.ec.simpl.common.model.dto.onboarding.OnboardingRequestDTO toV0(OnboardingRequestDTO onboardingRequestDTO);

    @Mapping(target = "ruleExecutions", ignore = true)
    DocumentDTO toV1(eu.europa.ec.simpl.common.model.dto.onboarding.DocumentDTO document);

    RejectDTO toV1(eu.europa.ec.simpl.api.onboarding.v1.model.RejectDTO rejectDTO);

    @BeanMapping(ignoreUnmappedSourceProperties = {"hasContent", "mandatory", "ruleExecutions"})
    eu.europa.ec.simpl.common.model.dto.onboarding.DocumentDTO toV0(DocumentDTO documentDTO);

    OnboardingRequestFilter toV0(SearchFilterParameterDTO filter);

    PageResponseOnboardingRequestDTO toV1(
            PageResponse<eu.europa.ec.simpl.common.model.dto.onboarding.OnboardingRequestDTO> search);

    default String toBase64String(byte[] bytes) {
        return Base64.getEncoder().encodeToString(bytes);
    }

    default byte[] toByteArray(String base64) {
        return Base64.getDecoder().decode(base64);
    }
}
