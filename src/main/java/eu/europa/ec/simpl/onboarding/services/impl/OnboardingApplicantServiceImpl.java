package eu.europa.ec.simpl.onboarding.services.impl;

import eu.europa.ec.simpl.common.constants.Roles;
import eu.europa.ec.simpl.common.model.dto.onboarding.OnboardingApplicantDTO;
import eu.europa.ec.simpl.common.model.dto.onboarding.ParticipantTypeDTO;
import eu.europa.ec.simpl.onboarding.entities.OnboardingApplicant;
import eu.europa.ec.simpl.onboarding.exceptions.OnboardingRequestAlreadyExistsException;
import eu.europa.ec.simpl.onboarding.mappers.OnboardingApplicantMapper;
import eu.europa.ec.simpl.onboarding.repositories.OnboardingApplicantRepository;
import eu.europa.ec.simpl.onboarding.services.OnboardingApplicantService;
import eu.europa.ec.simpl.onboarding.services.OnboardingTemplateService;
import eu.europa.ec.simpl.onboarding.services.ParticipantTypeService;
import eu.europa.ec.simpl.onboarding.services.UserService;
import jakarta.validation.constraints.NotNull;
import java.util.List;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

@Log4j2
@Service
@Validated
public class OnboardingApplicantServiceImpl implements OnboardingApplicantService {

    private final UserService userService;
    private final ParticipantTypeService participantTypeService;
    private final OnboardingTemplateService onboardingTemplateService;

    private final OnboardingApplicantRepository onboardingApplicantRepository;
    private final OnboardingApplicantMapper onboardingApplicantMapper;

    public OnboardingApplicantServiceImpl(
            UserService userService,
            OnboardingApplicantRepository onboardingApplicantRepository,
            ParticipantTypeService participantTypeService,
            OnboardingTemplateService onboardingTemplateService,
            OnboardingApplicantMapper onboardingApplicantMapper) {
        this.userService = userService;
        this.onboardingApplicantRepository = onboardingApplicantRepository;
        this.participantTypeService = participantTypeService;
        this.onboardingTemplateService = onboardingTemplateService;
        this.onboardingApplicantMapper = onboardingApplicantMapper;
    }

    @Override
    @Transactional
    public OnboardingApplicant create(
            @NotNull OnboardingApplicantDTO applicantDTO,
            @NotNull ParticipantTypeDTO pType,
            @NotNull String organization) {
        var participantType = participantTypeService.findParticipantTypeById(pType.getId());
        var existingRequests = findOnboardingApplicant(applicantDTO.getEmail());
        if (!existingRequests.isEmpty()) {
            throw new OnboardingRequestAlreadyExistsException(applicantDTO.getEmail());
        }
        onboardingTemplateService.findOnboardingTemplate(participantType.getId());
        userService.createUser(onboardingApplicantMapper.toKeycloakUserDto(applicantDTO, List.of(Roles.APPLICANT)));
        var entity = onboardingApplicantMapper.toEntity(applicantDTO, participantType, organization);
        return onboardingApplicantRepository.saveAndFlush(entity);
    }

    @Override
    public List<OnboardingApplicantDTO> findOnboardingApplicant(String applicantEmail) {
        return onboardingApplicantRepository.findByEmail(applicantEmail).stream()
                .map(onboardingApplicantMapper::toDto)
                .toList();
    }
}
