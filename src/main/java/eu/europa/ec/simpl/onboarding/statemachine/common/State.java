package eu.europa.ec.simpl.onboarding.statemachine.common;

public interface State {
    Type getType();

    enum Type {
        FINAL,
        INITIAL,
        STATE
    }
}
