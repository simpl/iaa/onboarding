package eu.europa.ec.simpl.onboarding.services;

import eu.europa.ec.simpl.common.model.dto.onboarding.OnboardingTemplateDTO;
import java.util.List;
import java.util.UUID;

public interface OnboardingTemplateService {

    OnboardingTemplateDTO setOnboardingTemplate(UUID participantTypeId, OnboardingTemplateDTO dto);

    void updateOnboardingTemplate(UUID participantType, List<UUID> documentsUuid);

    OnboardingTemplateDTO findOnboardingTemplate(UUID participantTypeId);

    void deleteOnboardingTemplate(UUID participantTypeId);

    List<OnboardingTemplateDTO> getOnboardingTemplates();
}
