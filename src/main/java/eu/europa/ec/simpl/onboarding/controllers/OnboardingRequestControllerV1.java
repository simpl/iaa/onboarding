package eu.europa.ec.simpl.onboarding.controllers;

import eu.europa.ec.simpl.api.onboarding.v1.exchanges.OnboardingRequestsManagementApi;
import eu.europa.ec.simpl.api.onboarding.v1.model.ApproveDTO;
import eu.europa.ec.simpl.api.onboarding.v1.model.CommentDTO;
import eu.europa.ec.simpl.api.onboarding.v1.model.DocumentDTO;
import eu.europa.ec.simpl.api.onboarding.v1.model.OnboardingRequestDTO;
import eu.europa.ec.simpl.api.onboarding.v1.model.PageResponseOnboardingRequestDTO;
import eu.europa.ec.simpl.api.onboarding.v1.model.RejectDTO;
import eu.europa.ec.simpl.api.onboarding.v1.model.SearchFilterParameterDTO;
import eu.europa.ec.simpl.onboarding.mappers.OnboardingRequestMapperV1;
import java.util.List;
import java.util.UUID;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("v1")
public class OnboardingRequestControllerV1 implements OnboardingRequestsManagementApi {

    private final OnboardingRequestController controller;
    private final OnboardingRequestMapperV1 mapper;

    public OnboardingRequestControllerV1(OnboardingRequestController controller, OnboardingRequestMapperV1 mapper) {
        this.controller = controller;
        this.mapper = mapper;
    }

    @Override
    public OnboardingRequestDTO addComment(UUID id, CommentDTO commentDTO) {
        return mapper.toV1(controller.addComment(id, mapper.toV0(commentDTO)));
    }

    @Override
    public OnboardingRequestDTO approve(UUID id, ApproveDTO approveDTO) {
        return mapper.toV1(controller.approve(id, mapper.toV0(approveDTO)));
    }

    @Override
    public OnboardingRequestDTO create(OnboardingRequestDTO onboardingRequestDTO) {
        return mapper.toV1(controller.create(mapper.toV0(onboardingRequestDTO)));
    }

    @Override
    public void deleteDocument(UUID onboardingRequestId, UUID documentId) {
        controller.deleteDocument(onboardingRequestId, documentId);
    }

    @Override
    public DocumentDTO getDocument(UUID onboardingRequestId, UUID documentId) {
        return mapper.toV1(controller.getDocument(onboardingRequestId, documentId));
    }

    @Override
    public OnboardingRequestDTO getOnboardingRequest(UUID onboardingRequestId) {
        return mapper.toV1(controller.getOnboardingRequest(onboardingRequestId));
    }

    @Override
    public OnboardingRequestDTO reject(UUID id, RejectDTO rejectDTO) {
        return mapper.toV1(controller.reject(id, mapper.toV1(rejectDTO)));
    }

    @Override
    public OnboardingRequestDTO requestAdditionalDocument(UUID id, DocumentDTO documentDTO) {
        return mapper.toV1(controller.requestAdditionalDocument(id, mapper.toV0(documentDTO)));
    }

    @Override
    public OnboardingRequestDTO requestRevision(UUID id) {
        return mapper.toV1(controller.requestRevision(id));
    }

    @Override
    public PageResponseOnboardingRequestDTO search(
            Integer page, Integer size, List<String> sort, SearchFilterParameterDTO filter) {
        return mapper.toV1(controller.search(
                mapper.toV0(filter), PageRequest.of(page, size, Sort.by(sort.toArray(String[]::new)))));
    }

    @Override
    public void setExpirationTimeframe(UUID id, Long expirationTimeframe) {
        controller.setExpirationTimeframe(id, expirationTimeframe);
    }

    @Override
    public OnboardingRequestDTO submit(UUID id) {
        return mapper.toV1(controller.submit(id));
    }

    @Override
    public OnboardingRequestDTO uploadDocument(UUID id, DocumentDTO documentDTO) {
        return mapper.toV1(controller.uploadDocument(id, mapper.toV0(documentDTO)));
    }
}
