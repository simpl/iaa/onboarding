package eu.europa.ec.simpl.onboarding.controllers;

import eu.europa.ec.simpl.common.model.dto.onboarding.OnboardingTemplateDTO;
import eu.europa.ec.simpl.onboarding.exceptions.OnboardingTemplateNotFoundException;
import eu.europa.ec.simpl.onboarding.services.OnboardingTemplateService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import jakarta.validation.Valid;
import java.util.List;
import java.util.UUID;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("onboarding-template")
public class OnboardingTemplateController {

    private final OnboardingTemplateService onboardingTemplateService;

    public OnboardingTemplateController(OnboardingTemplateService onboardingTemplateService) {
        this.onboardingTemplateService = onboardingTemplateService;
    }

    @Operation(
            summary = "Update Onboarding Template",
            description = "Updates the onboarding template for a specific participant type.",
            parameters = {
                @Parameter(
                        name = "participantTypeId",
                        description = "Type of participant for which the onboarding template is updated",
                        required = true,
                        schema = @Schema(type = "string", format = "uuid"))
            },
            requestBody =
                    @io.swagger.v3.oas.annotations.parameters.RequestBody(
                            description = "Onboarding template data to be updated",
                            required = true,
                            content =
                                    @Content(
                                            mediaType = "application/json",
                                            schema = @Schema(implementation = OnboardingTemplateDTO.class))),
            responses = {
                @ApiResponse(
                        responseCode = "200",
                        description = "Onboarding template updated successfully",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = OnboardingTemplateDTO.class))),
                @ApiResponse(responseCode = "400", description = "Invalid input data"),
                @ApiResponse(responseCode = "401", description = "Access denied"),
                @ApiResponse(responseCode = "403", description = "Forbidden: User does not have the required role"),
                @ApiResponse(responseCode = "404", description = "Participant type not found")
            })
    @PutMapping("{participantTypeId}")
    public OnboardingTemplateDTO updateOnboardingTemplate(
            @PathVariable UUID participantTypeId, @RequestBody @Valid OnboardingTemplateDTO onboardingTemplate) {
        return onboardingTemplateService.setOnboardingTemplate(participantTypeId, onboardingTemplate);
    }

    @Operation(
            summary = "Get Onboarding Templates",
            description = "Retrieves a list of all onboarding templates.",
            responses = {
                @ApiResponse(
                        responseCode = "200",
                        description = "List of onboarding templates retrieved successfully",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        array =
                                                @ArraySchema(
                                                        schema =
                                                                @Schema(
                                                                        implementation =
                                                                                OnboardingTemplateDTO.class)))),
                @ApiResponse(responseCode = "401", description = "Access denied"),
                @ApiResponse(responseCode = "403", description = "Forbidden: User does not have the required role")
            })
    @GetMapping
    public List<OnboardingTemplateDTO> getOnboardingTemplates() {
        return onboardingTemplateService.getOnboardingTemplates();
    }

    @Operation(
            summary = "Get Onboarding Template by Participant Type",
            description = "Retrieves an onboarding template for a specific participant type.",
            parameters = {
                @Parameter(
                        name = "participantTypeId",
                        description = "Type of participant for which the onboarding template is retrieved",
                        required = true,
                        schema = @Schema(type = "string", format = "uuid"))
            },
            responses = {
                @ApiResponse(
                        responseCode = "200",
                        description = "Onboarding template retrieved successfully",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = OnboardingTemplateDTO.class))),
                @ApiResponse(
                        responseCode = "204",
                        description = "Onboarding template not present for the specified participant type"),
                @ApiResponse(responseCode = "401", description = "Access denied"),
                @ApiResponse(responseCode = "403", description = "Forbidden: User does not have the required role"),
                @ApiResponse(responseCode = "404", description = "Participant type not found")
            })
    @GetMapping("{participantTypeId}")
    public ResponseEntity<OnboardingTemplateDTO> getOnboardingTemplate(@PathVariable UUID participantTypeId) {
        try {
            return new ResponseEntity<>(
                    onboardingTemplateService.findOnboardingTemplate(participantTypeId), HttpStatus.OK);
        } catch (OnboardingTemplateNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @Operation(
            summary = "Update Onboarding Template",
            description =
                    "Updates the onboarding template for a specific participant type with a list of document template UUIDs.",
            parameters = {
                @Parameter(
                        name = "participantTypeId",
                        description = "Type of participant for which the onboarding template is updated",
                        required = true,
                        schema = @Schema(type = "string", format = "uuid"))
            },
            requestBody =
                    @io.swagger.v3.oas.annotations.parameters.RequestBody(
                            description =
                                    "List of document template UUIDs to be associated with the onboarding template",
                            required = true,
                            content =
                                    @Content(array = @ArraySchema(schema = @Schema(type = "string", format = "uuid")))),
            responses = {
                @ApiResponse(responseCode = "200", description = "Onboarding template updated successfully"),
                @ApiResponse(responseCode = "400", description = "Invalid input data"),
                @ApiResponse(responseCode = "401", description = "Access denied"),
                @ApiResponse(responseCode = "403", description = "Forbidden: User does not have the required role"),
                @ApiResponse(responseCode = "404", description = "Participant type not found")
            })
    @PatchMapping("{participantTypeId}")
    public void updateOnboardingTemplate(
            @PathVariable(name = "participantTypeId") UUID participantTypeId, @RequestBody List<UUID> uuids) {
        onboardingTemplateService.updateOnboardingTemplate(participantTypeId, uuids);
    }

    @Operation(
            summary = "Delete Onboarding Template",
            description = "Deletes the onboarding template for a specific participant type.",
            parameters = {
                @Parameter(
                        name = "participantTypeId",
                        description = "Type of participant for which the onboarding template is deleted",
                        required = true,
                        schema = @Schema(type = "string", format = "uuid"))
            },
            responses = {
                @ApiResponse(responseCode = "204", description = "Onboarding template deleted successfully"),
                @ApiResponse(responseCode = "401", description = "Access denied"),
                @ApiResponse(responseCode = "403", description = "Forbidden: User does not have the required role"),
                @ApiResponse(responseCode = "404", description = "Participant type not found")
            })
    @DeleteMapping("{participantTypeId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteOnboardingTemplate(@PathVariable UUID participantTypeId) {
        onboardingTemplateService.deleteOnboardingTemplate(participantTypeId);
    }
}
