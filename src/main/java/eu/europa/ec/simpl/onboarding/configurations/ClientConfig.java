package eu.europa.ec.simpl.onboarding.configurations;

import eu.europa.ec.simpl.common.argumentresolvers.PageableArgumentResolver;
import eu.europa.ec.simpl.common.argumentresolvers.QueryParamsArgumentResolver;
import eu.europa.ec.simpl.common.exchanges.identityprovider.IdentityProviderParticipantExchange;
import eu.europa.ec.simpl.common.exchanges.usersroles.UserExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestClient;
import org.springframework.web.client.support.RestClientAdapter;
import org.springframework.web.service.invoker.HttpServiceProxyFactory;

@Configuration
public class ClientConfig {

    @Bean
    public UserExchange userExchange(MicroserviceProperties properties, RestClient.Builder restClientBuilder) {
        return buildExchange(properties.usersRoles().url(), restClientBuilder, UserExchange.class);
    }

    @Bean
    public IdentityProviderParticipantExchange participantExchange(
            MicroserviceProperties properties, RestClient.Builder restClientBuilder) {
        return buildExchange(
                properties.identityProvider().url(), restClientBuilder, IdentityProviderParticipantExchange.class);
    }

    private <E> E buildExchange(String baseurl, RestClient.Builder restClientBuilder, Class<E> clazz) {
        var restClient = restClientBuilder.baseUrl(baseurl).build();
        var adapter = RestClientAdapter.create(restClient);
        var factory = HttpServiceProxyFactory.builderFor(adapter)
                .customArgumentResolver(new PageableArgumentResolver())
                .customArgumentResolver(new QueryParamsArgumentResolver())
                .build();
        return factory.createClient(clazz);
    }
}
