package eu.europa.ec.simpl.onboarding.liquibase.migration_2024_11_25.pojo;

import lombok.Data;

@Data
public class ParticipantType {

    private Long id;
    private String value;
}
