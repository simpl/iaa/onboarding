package eu.europa.ec.simpl.onboarding.services.impl;

import eu.europa.ec.simpl.common.model.dto.onboarding.DocumentDTO;
import eu.europa.ec.simpl.onboarding.mappers.DocumentMapper;
import eu.europa.ec.simpl.onboarding.repositories.DocumentRepository;
import eu.europa.ec.simpl.onboarding.services.DocumentService;
import java.util.List;
import java.util.UUID;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

@Log4j2
@Service
@Validated
public class DocumentServiceImpl implements DocumentService {

    private final DocumentMapper documentMapper;
    private final DocumentRepository documentRepository;

    public DocumentServiceImpl(DocumentMapper documentMapper, DocumentRepository documentRepository) {
        this.documentMapper = documentMapper;
        this.documentRepository = documentRepository;
    }

    @Override
    @Transactional
    public DocumentDTO createDocument(DocumentDTO document, UUID onboardingRequestId) {
        var entity = documentRepository.saveAndFlush(documentMapper.toEntity(document, onboardingRequestId));
        return documentMapper.toDTO(entity);
    }

    @Override
    @Transactional
    public List<DocumentDTO> createDocuments(List<DocumentDTO> documents, UUID onboardingRequestId) {
        return documentRepository
                .saveAllAndFlush(documents.stream()
                        .map(dto -> documentMapper.toEntity(dto, onboardingRequestId))
                        .toList())
                .stream()
                .map(documentMapper::toDTO)
                .toList();
    }

    @Override
    @Transactional
    public void uploadDocument(DocumentDTO document) {
        var entity = documentRepository.findByIdOrThrow(document.getId());
        documentMapper.updateDocument(entity, document);
    }

    @Override
    @Transactional
    public DocumentDTO getDocument(UUID documentId) {
        var document = documentRepository.findFullByIdOrThrow(documentId);
        return documentMapper.toFullDTO(document);
    }

    @Transactional
    @Override
    public void clearDocumentContent(UUID documentId) {
        var entity = documentRepository.findByIdOrThrow(documentId);
        entity.setContent(null);
        entity.setFilename(null);
        entity.setFilesize(null);
    }
}
