package eu.europa.ec.simpl.onboarding.exceptions;

import eu.europa.ec.simpl.common.exceptions.StatusException;
import java.util.UUID;
import org.springframework.http.HttpStatus;

public class CannotEditOnboardingRequestException extends StatusException {
    public CannotEditOnboardingRequestException(String user, UUID onboardingRequestId) {
        this(
                HttpStatus.FORBIDDEN,
                "User %s is not allowed to edit onboarding request %s".formatted(user, onboardingRequestId));
    }

    public CannotEditOnboardingRequestException(HttpStatus status, String message) {
        super(status, message);
    }
}
