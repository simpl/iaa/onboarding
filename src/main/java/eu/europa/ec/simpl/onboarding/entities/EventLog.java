package eu.europa.ec.simpl.onboarding.entities;

import eu.europa.ec.simpl.common.entity.annotations.UUIDv7Generator;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import java.time.Instant;
import java.util.UUID;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.type.SqlTypes;

@Getter
@Setter
@Entity
@Accessors(chain = true)
@ToString
@Table(name = "event_log")
@NoArgsConstructor
public class EventLog {

    @Id
    @UUIDv7Generator
    @Column(name = "id", nullable = false)
    private UUID id;

    @Column(name = "onboarding_request_id", nullable = false)
    private UUID onboardingRequestId;

    @Column(name = "initiator_user_id")
    private UUID initiatorUserId;

    @Column(name = "initiator_service", length = 50)
    private String initiatorService;

    @Column(name = "event_type", nullable = false, length = 50)
    private String eventType;

    @ToString.Exclude
    @Column(name = "event_details")
    @JdbcTypeCode(SqlTypes.JSON)
    private String eventDetails;

    @Column(name = "entity_id")
    private UUID entityId;

    @CreationTimestamp
    @Column(name = "creation_timestamp", nullable = false)
    private Instant creationTimestamp;
}
