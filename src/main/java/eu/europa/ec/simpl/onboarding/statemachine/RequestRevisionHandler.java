package eu.europa.ec.simpl.onboarding.statemachine;

import eu.europa.ec.simpl.common.security.JwtService;
import org.springframework.stereotype.Component;

@Component
public class RequestRevisionHandler implements RequestRevision, NotaryEvent<Void> {

    private final JwtService jwtService;
    private final StatusChangeHandler statusChangeHandler;

    protected RequestRevisionHandler(JwtService jwtService, StatusChangeHandler statusChangeHandler) {
        this.jwtService = jwtService;
        this.statusChangeHandler = statusChangeHandler;
    }

    @Override
    public void handle(OnboardingState source, OnboardingState target, OnboardingPayload<Void> payload) {
        statusChangeHandler.changeStatus(payload.getOnboardingRequest(), target);
    }

    @Override
    public boolean guard(OnboardingState source, OnboardingState target, OnboardingPayload<Void> payload) {
        return NotaryEvent.super.guard(source, target, payload);
    }

    @Override
    public JwtService getJwtService() {
        return jwtService;
    }
}
