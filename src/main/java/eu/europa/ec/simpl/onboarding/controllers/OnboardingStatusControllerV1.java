package eu.europa.ec.simpl.onboarding.controllers;

import eu.europa.ec.simpl.api.onboarding.v1.exchanges.OnboardingStatusesApi;
import eu.europa.ec.simpl.api.onboarding.v1.model.OnboardingStatusDTO;
import eu.europa.ec.simpl.onboarding.mappers.OnboardingStatusMapperV1;
import java.util.List;
import java.util.UUID;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("v1")
public class OnboardingStatusControllerV1 implements OnboardingStatusesApi {

    private final OnboardingStatusController controller;
    private final OnboardingStatusMapperV1 mapper;

    public OnboardingStatusControllerV1(OnboardingStatusController controller, OnboardingStatusMapperV1 mapper) {
        this.controller = controller;
        this.mapper = mapper;
    }

    @Override
    public List<OnboardingStatusDTO> getStatus() {
        return mapper.toV1(controller.getStatus());
    }

    @Override
    public void setLabel(UUID id, String label) {
        controller.setLabel(id, label);
    }
}
