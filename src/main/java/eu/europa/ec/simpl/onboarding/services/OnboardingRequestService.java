package eu.europa.ec.simpl.onboarding.services;

import eu.europa.ec.simpl.common.filters.OnboardingRequestFilter;
import eu.europa.ec.simpl.common.model.dto.onboarding.ApproveDTO;
import eu.europa.ec.simpl.common.model.dto.onboarding.CommentDTO;
import eu.europa.ec.simpl.common.model.dto.onboarding.DocumentDTO;
import eu.europa.ec.simpl.common.model.dto.onboarding.OnboardingRequestDTO;
import eu.europa.ec.simpl.common.model.dto.onboarding.RejectDTO;
import eu.europa.ec.simpl.common.responses.PageResponse;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.PositiveOrZero;
import java.util.UUID;
import org.springframework.data.domain.Pageable;

public interface OnboardingRequestService {

    OnboardingRequestDTO create(@Valid OnboardingRequestDTO onboardingRequestDTO);

    OnboardingRequestDTO submit(UUID onboardingRequestId, @NotBlank String initiator);

    OnboardingRequestDTO approve(UUID onboardingRequestId, ApproveDTO approveDTO, String initiator);

    OnboardingRequestDTO reject(UUID onboardingRequestId, @Valid RejectDTO rejectDTO, @NotBlank String initiator);

    OnboardingRequestDTO uploadDocument(
            UUID onboardingRequestId, @Valid DocumentDTO documentDTO, @NotBlank String initiator);

    OnboardingRequestDTO requestAdditionalDocument(UUID onboardingRequestId, DocumentDTO documentDTO, String initiator);

    OnboardingRequestDTO requestRevision(UUID onboardingRequestId, String initiator);

    OnboardingRequestDTO addComment(UUID onboardingRequestId, CommentDTO commentDTO, String initiator);

    PageResponse<OnboardingRequestDTO> search(OnboardingRequestFilter filter, Pageable pageable);

    void setExpirationTimeframe(UUID id, @PositiveOrZero Long expirationTimeframe);

    DocumentDTO getDocument(UUID onboardingRequestId, UUID documentId);

    OnboardingRequestDTO getOnboardingRequest(UUID onboardingRequestId);

    void deleteDocument(UUID onboardingRequestId, UUID documentId);
}
