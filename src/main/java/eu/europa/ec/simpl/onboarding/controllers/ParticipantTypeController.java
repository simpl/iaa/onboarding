package eu.europa.ec.simpl.onboarding.controllers;

import eu.europa.ec.simpl.common.exchanges.onboarding.ParticipantTypeExchange;
import eu.europa.ec.simpl.common.model.dto.onboarding.ParticipantTypeDTO;
import eu.europa.ec.simpl.onboarding.services.ParticipantTypeService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import java.util.Set;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ParticipantTypeController implements ParticipantTypeExchange {

    private final ParticipantTypeService service;

    public ParticipantTypeController(ParticipantTypeService service) {
        this.service = service;
    }

    @Operation(
            summary = "Get all participant types",
            description = "Returns a set of all participant types available in the system.")
    @ApiResponses(
            value = {
                @ApiResponse(
                        responseCode = "200",
                        description = "Successfully retrieved participant types",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        array =
                                                @ArraySchema(
                                                        schema = @Schema(implementation = ParticipantTypeDTO.class)))),
                @ApiResponse(responseCode = "401", description = "Access denied"),
                @ApiResponse(responseCode = "403", description = "Forbidden: User does not have the required role")
            })
    @Override
    public Set<ParticipantTypeDTO> getParticipantTypes() {
        return service.findAllParticipantTypes();
    }
}
