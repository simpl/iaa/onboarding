package eu.europa.ec.simpl.onboarding.statemachine.common;

import java.util.Set;

public interface BasicEvent<S extends State, P> extends Event<S, P> {

    @Override
    default Set<Edge<S>> getEdges() {
        return Set.of(Edge.of(getSource(), getTarget()));
    }

    S getSource();

    S getTarget();
}
