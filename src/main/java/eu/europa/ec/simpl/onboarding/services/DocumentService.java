package eu.europa.ec.simpl.onboarding.services;

import eu.europa.ec.simpl.common.model.dto.onboarding.DocumentDTO;
import jakarta.validation.Valid;
import java.util.List;
import java.util.UUID;

public interface DocumentService {

    DocumentDTO createDocument(DocumentDTO document, UUID onboardingRequestId);

    List<DocumentDTO> createDocuments(List<DocumentDTO> documents, UUID onboardingRequestId);

    void uploadDocument(@Valid DocumentDTO document);

    DocumentDTO getDocument(UUID documentId);

    void clearDocumentContent(UUID documentId);
}
