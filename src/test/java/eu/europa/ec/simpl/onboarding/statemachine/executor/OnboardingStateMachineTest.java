package eu.europa.ec.simpl.onboarding.statemachine.executor;

import static eu.europa.ec.simpl.common.test.TestUtil.an;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

import eu.europa.ec.simpl.common.model.dto.onboarding.OnboardingStatusDTO;
import eu.europa.ec.simpl.common.security.JwtService;
import eu.europa.ec.simpl.onboarding.entities.OnboardingRequest;
import eu.europa.ec.simpl.onboarding.mappers.OnboardingRequestMapper;
import eu.europa.ec.simpl.onboarding.mappers.ReferenceMapper;
import eu.europa.ec.simpl.onboarding.repositories.OnboardingRequestRepository;
import eu.europa.ec.simpl.onboarding.services.OnboardingStatusService;
import eu.europa.ec.simpl.onboarding.statemachine.OnboardingStateMapper;
import eu.europa.ec.simpl.onboarding.statemachine.TimeoutEvent;
import eu.europa.ec.simpl.onboarding.statemachine.TimeoutEventHandler;
import eu.europa.ec.simpl.onboarding.statemachine.listener.OnboardingStateListener;
import java.util.UUID;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.bean.override.mockito.MockitoBean;
import org.springframework.test.context.bean.override.mockito.MockitoSpyBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@Import({
    OnboardingStateMachine.class,
    TimeoutEventHandler.class,
})
class OnboardingStateMachineTest {

    @MockitoBean
    private OnboardingStateMapper onboardingStateMapper;

    @MockitoBean
    private ReferenceMapper referenceMapper;

    @MockitoBean
    private OnboardingStatusService onboardingStatusService;

    @MockitoBean
    private OnboardingStateListener stateListener;

    @MockitoBean
    private OnboardingRequestRepository onboardingRequestRepository;

    @MockitoBean
    private OnboardingRequestMapper onboardingRequestMapper;

    @MockitoBean
    private JwtService jwtService;

    @Autowired
    private OnboardingStateMachine onboardingStateMachine;

    @MockitoSpyBean
    private TimeoutEventHandler timeoutEventHandler;

    @Test
    void loadContext() {
        var onboardingRequestId = UUID.randomUUID();
        var event = TimeoutEvent.class;
        var onboardingRequest = an(OnboardingRequest.class);
        doReturn(true).when(timeoutEventHandler).guard(any(), any(), any());
        doNothing().when(timeoutEventHandler).handle(any(), any(), any());
        onboardingRequest.setId(onboardingRequestId);
        when(onboardingRequestRepository.findByIdOrThrow(onboardingRequestId)).thenReturn(onboardingRequest);
        var onboardingStatusDTO = an(OnboardingStatusDTO.class);
        onboardingStatusDTO.setId(onboardingRequestId);
        when(onboardingStatusService.findOnboardingStatusByStatus(any())).thenReturn(onboardingStatusDTO);
        onboardingStateMachine.execute(onboardingRequestId, event, "junit-tests");
    }
}
