package eu.europa.ec.simpl.onboarding.repositories;

import static eu.europa.ec.simpl.common.test.TestUtil.anOptional;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.BDDAssertions.catchException;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

import eu.europa.ec.simpl.onboarding.entities.DocumentTemplate;
import eu.europa.ec.simpl.onboarding.exceptions.DocumentTemplateNotFoundException;
import java.util.Optional;
import java.util.UUID;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class DocumentTemplateRepositoryTest {
    @Spy
    DocumentTemplateRepository documentTemplateRepository;

    @Test
    void findByIdOrThrow_whenFindByIdReturnsEntity_shouldNotThrowException() {
        given(documentTemplateRepository.findById(any(UUID.class))).willReturn(anOptional(DocumentTemplate.class));
        var exception = catchException(() -> documentTemplateRepository.findByIdOrThrow(UUID.randomUUID()));
        assertThat(exception).isNull();
    }

    @Test
    void findByIdOrThrow_whenFindByIdReturnsEntity_shouldThrowException() {
        given(documentTemplateRepository.findById(any(UUID.class))).willReturn(Optional.empty());
        var exception = catchException(() -> documentTemplateRepository.findByIdOrThrow(UUID.randomUUID()));
        assertThat(exception).isInstanceOf(DocumentTemplateNotFoundException.class);
    }
}
