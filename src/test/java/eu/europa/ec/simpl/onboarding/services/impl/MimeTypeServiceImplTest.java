package eu.europa.ec.simpl.onboarding.services.impl;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

import eu.europa.ec.simpl.common.model.dto.onboarding.MimeTypeDTO;
import eu.europa.ec.simpl.onboarding.entities.MimeType;
import eu.europa.ec.simpl.onboarding.exceptions.MimeTypeNotFoundException;
import eu.europa.ec.simpl.onboarding.exceptions.UnsupportedMimeTypeException;
import eu.europa.ec.simpl.onboarding.mappers.MimeTypeMapper;
import eu.europa.ec.simpl.onboarding.repositories.MimeTypeRepository;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.instancio.junit.InstancioSource;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

@ExtendWith(MockitoExtension.class)
class MimeTypeServiceImplTest {

    @Mock
    private MimeTypeRepository mimeTypeRepository;

    @Mock
    private MimeTypeMapper mimeTypeMapper;

    @InjectMocks
    private MimeTypeServiceImpl mimeTypeService;

    @ParameterizedTest
    @InstancioSource
    void findMimeTypeByValue_WhenExists_ReturnsEntity(MimeType mimeType) {
        given(mimeTypeRepository.findMimeTypeByValue(anyString())).willReturn(Optional.of(mimeType));

        MimeType result = mimeTypeService.findMimeTypeByValue("application/json");

        assertThat(result).isEqualTo(mimeType);
        verify(mimeTypeRepository).findMimeTypeByValue("application/json");
    }

    @Test
    void findMimeTypeByValue_WhenNotExists_ThrowsException() {
        given(mimeTypeRepository.findMimeTypeByValue(anyString())).willReturn(Optional.empty());

        assertThatThrownBy(() -> mimeTypeService.findMimeTypeByValue("invalid"))
                .isInstanceOf(MimeTypeNotFoundException.class)
                .hasMessageContaining("invalid");
    }

    @ParameterizedTest
    @InstancioSource
    void createMimeType_SavesAndReturnsId(MimeType mimeType, MimeTypeDTO mimeTypeDTO) {
        given(mimeTypeMapper.toEntity(any(MimeTypeDTO.class))).willReturn(mimeType);
        given(mimeTypeRepository.save(any(MimeType.class))).willReturn(mimeType);

        UUID result = mimeTypeService.createMimeType(mimeTypeDTO.setValue("application/json"));

        assertThat(result).isEqualTo(mimeType.getId());
        verify(mimeTypeMapper).toEntity(mimeTypeDTO);
        verify(mimeTypeRepository).save(mimeType);
    }

    @ParameterizedTest
    @InstancioSource
    void createMimeType_whenInvalidMimeType_shouldThrowUnsupportedMimeTypeException(MimeTypeDTO mimeTypeDTO) {
        var exception = catchException(() -> mimeTypeService.createMimeType(mimeTypeDTO.setValue("invalidMime")));
        assertThat(exception).isInstanceOf(UnsupportedMimeTypeException.class);
    }

    @ParameterizedTest
    @InstancioSource
    void search_ReturnsPageOfDTOs(MimeType mimeType, MimeTypeDTO mimeTypeDTO) {
        Pageable pageable = PageRequest.of(0, 10);
        Page<MimeType> page = new PageImpl<>(List.of(mimeType));
        given(mimeTypeRepository.findAll(pageable)).willReturn(page);
        given(mimeTypeMapper.toDTO(any(MimeType.class))).willReturn(mimeTypeDTO);

        Page<MimeTypeDTO> result = mimeTypeService.search(pageable);

        assertThat(result).hasSize(1);
        assertThat(result.getContent().get(0)).isEqualTo(mimeTypeDTO);
        verify(mimeTypeRepository).findAll(pageable);
        verify(mimeTypeMapper).toDTO(mimeType);
    }

    @ParameterizedTest
    @InstancioSource
    void findById_WhenExists_ReturnsDTO(MimeType mimeType, MimeTypeDTO mimeTypeDTO) {
        var uuid = UUID.randomUUID();
        given(mimeTypeRepository.findById(uuid)).willReturn(Optional.of(mimeType));
        given(mimeTypeMapper.toDTO(any(MimeType.class))).willReturn(mimeTypeDTO);

        MimeTypeDTO result = mimeTypeService.findById(uuid);

        assertThat(result).isEqualTo(mimeTypeDTO);
        verify(mimeTypeRepository).findById(uuid);
        verify(mimeTypeMapper).toDTO(mimeType);
    }

    @Test
    void findById_WhenNotExists_ThrowsException() {
        var uuid = UUID.randomUUID();
        given(mimeTypeRepository.findById(uuid)).willReturn(Optional.empty());

        assertThatThrownBy(() -> mimeTypeService.findById(uuid))
                .isInstanceOf(MimeTypeNotFoundException.class)
                .hasMessageContaining(uuid.toString());
    }

    @ParameterizedTest
    @InstancioSource
    void updateById_WhenExists_UpdatesEntity(MimeType mimeType, MimeTypeDTO mimeTypeDTO) {
        var uuid = UUID.randomUUID();
        given(mimeTypeRepository.findById(uuid)).willReturn(Optional.of(mimeType));

        mimeTypeService.updateById(uuid, mimeTypeDTO);

        verify(mimeTypeRepository).findById(uuid);
        verify(mimeTypeMapper).updateEntity(mimeType, mimeTypeDTO);
    }

    @ParameterizedTest
    @InstancioSource
    void updateById_WhenNotExists_ThrowsException(MimeTypeDTO mimeTypeDTO) {
        var uuid = UUID.randomUUID();
        given(mimeTypeRepository.findById(uuid)).willReturn(Optional.empty());

        assertThatThrownBy(() -> mimeTypeService.updateById(uuid, mimeTypeDTO))
                .isInstanceOf(MimeTypeNotFoundException.class)
                .hasMessageContaining(uuid.toString());
    }

    @Test
    void deleteById_CallsRepository() {
        var uuid = UUID.randomUUID();
        mimeTypeService.deleteById(uuid);

        verify(mimeTypeRepository).deleteById(uuid);
    }
}
