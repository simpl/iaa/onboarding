package eu.europa.ec.simpl.onboarding.statemachine;

import static eu.europa.ec.simpl.common.test.TestUtil.an;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mockStatic;

import eu.europa.ec.simpl.common.model.dto.onboarding.DocumentDTO;
import eu.europa.ec.simpl.common.security.JwtService;
import eu.europa.ec.simpl.onboarding.entities.Document;
import eu.europa.ec.simpl.onboarding.entities.OnboardingRequest;
import eu.europa.ec.simpl.onboarding.exceptions.InvalidDocumentException;
import eu.europa.ec.simpl.onboarding.services.DocumentService;
import eu.europa.ec.simpl.onboarding.utils.MimeTypeUtil;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.bean.override.mockito.MockitoBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@Import(UploadDocumentHandler.class)
class UploadDocumentHandlerTest {

    @MockitoBean
    private JwtService jwtService;

    @MockitoBean
    private DocumentService documentService;

    @Autowired
    private UploadDocumentHandler handler;

    @Test
    void handleTest_success() {
        var source = OnboardingState.IN_PROGRESS;
        var target = OnboardingState.IN_PROGRESS;
        var request = an(OnboardingRequest.class);
        var document = an(Document.class);
        request.setDocuments(List.of(document));
        var documentDto = an(DocumentDTO.class);
        documentDto.setId(document.getId());
        var payload = new OnboardingPayload<>(request, documentDto);
        try (var mimeTypeHelper = mockStatic(MimeTypeUtil.class)) {
            given(MimeTypeUtil.hasMatchingMimeType(any(String.class), any(String.class)))
                    .willReturn(true);
            handler.handle(source, target, payload);
        }
    }

    @Test
    void handleTest_invalidDocumentException() {
        var source = OnboardingState.IN_PROGRESS;
        var target = OnboardingState.IN_PROGRESS;
        var request = an(OnboardingRequest.class);
        var document = an(Document.class);
        request.setDocuments(List.of(document));
        var documentDto = an(DocumentDTO.class);
        var payload = new OnboardingPayload<>(request, documentDto);
        assertThrows(InvalidDocumentException.class, () -> handler.handle(source, target, payload));
    }

    @Test
    void handleTest_InvalidMymeType_InvalidDocumentsException() {
        var source = OnboardingState.IN_PROGRESS;
        var target = OnboardingState.IN_PROGRESS;
        var request = an(OnboardingRequest.class);
        var document = an(Document.class);
        request.setDocuments(List.of(document));
        var documentDto = an(DocumentDTO.class);
        documentDto.setId(document.getId());
        var payload = new OnboardingPayload<>(request, documentDto);
        try (var mimeTypeHelper = mockStatic(MimeTypeUtil.class)) {
            given(MimeTypeUtil.hasMatchingMimeType(any(String.class), any(String.class)))
                    .willReturn(false);
            assertThrows(InvalidDocumentException.class, () -> handler.handle(source, target, payload));
        }
    }

    @Test
    void getJwtServiceTest() {
        assertNotNull(handler.getJwtService());
    }
}
