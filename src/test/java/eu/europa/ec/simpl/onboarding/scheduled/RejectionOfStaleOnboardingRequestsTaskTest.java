package eu.europa.ec.simpl.onboarding.scheduled;

import static eu.europa.ec.simpl.common.test.TestUtil.a;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

import eu.europa.ec.simpl.onboarding.entities.OnboardingRequest;
import eu.europa.ec.simpl.onboarding.repositories.OnboardingRequestRepository;
import eu.europa.ec.simpl.onboarding.statemachine.executor.OnboardingStateMachine;
import java.util.List;
import java.util.UUID;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.bean.override.mockito.MockitoBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@Import(RejectionOfStaleOnboardingRequestsTask.class)
class RejectionOfStaleOnboardingRequestsTaskTest {

    @MockitoBean
    private OnboardingStateMachine onboardingSM;

    @MockitoBean
    OnboardingRequestRepository onboardingRequestRepo;

    @Autowired
    private RejectionOfStaleOnboardingRequestsTask task;

    @Autowired
    private OnboardingRequestRepository onboardingRequestRepository;

    @Autowired
    private OnboardingStateMachine onboardingStateMachine;

    @Test
    void rejectionOfStaleOnboardingRequestsTask_success() {
        var requestId1 = UUID.randomUUID();
        var requestId2 = UUID.randomUUID();
        var requests = List.of(request(requestId1), request(requestId2));
        given(onboardingRequestRepository.findInProgressWithExpiredTimeframe()).willReturn(requests);
        task.rejectionOfStaleOnboardingRequestsTask();
        verify(onboardingStateMachine, times(1)).execute(eq(requestId1), any(), any());
        verify(onboardingStateMachine, times(1)).execute(eq(requestId2), any(), any());
    }

    private OnboardingRequest request(UUID id) {
        var request = a(OnboardingRequest.class);
        request.setId(id);
        return request;
    }
}
