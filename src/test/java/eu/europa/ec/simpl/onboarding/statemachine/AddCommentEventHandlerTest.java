package eu.europa.ec.simpl.onboarding.statemachine;

import static eu.europa.ec.simpl.common.test.TestUtil.an;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;

import eu.europa.ec.simpl.common.constants.Roles;
import eu.europa.ec.simpl.common.model.dto.onboarding.CommentDTO;
import eu.europa.ec.simpl.common.security.JwtService;
import eu.europa.ec.simpl.onboarding.entities.Comment;
import eu.europa.ec.simpl.onboarding.entities.OnboardingRequest;
import eu.europa.ec.simpl.onboarding.exceptions.OperationNotPermittedException;
import eu.europa.ec.simpl.onboarding.mappers.ImportMapper;
import eu.europa.ec.simpl.onboarding.repositories.*;
import eu.europa.ec.simpl.onboarding.services.CommentService;
import java.util.ArrayList;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.bean.override.mockito.MockitoBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@Import(AddCommentEventHandler.class)
@ImportMapper
class AddCommentEventHandlerTest {

    @MockitoBean
    private OnboardingRequestRepository onboardingRequestRepository;

    @MockitoBean
    private OnboardingStatusRepository onboardingStatusRepository;

    @MockitoBean
    private OnboardingTemplateRepository onboardingTemplateRepository;

    @MockitoBean
    private DocumentTemplateRepository documentTemplateRepository;

    @MockitoBean
    private DocumentRepository documentRepository;

    @MockitoBean
    private CommentRepository commentRepository;

    @MockitoBean
    private ParticipantTypeRepository participantTypeRepository;

    @MockitoBean
    private MimeTypeRepository mimeTypeRepository;

    @MockitoBean
    private JwtService jwtService;

    @MockitoBean
    private CommentService commentService;

    @Autowired
    private AddCommentEventHandler handler;

    @Test
    void handleTest() {
        var source = OnboardingState.IN_PROGRESS;
        var target = OnboardingState.IN_PROGRESS;

        var actionPayload = an(CommentDTO.class);
        var onboardingRequest = an(OnboardingRequest.class);
        var comment = an(Comment.class);
        onboardingRequest.setComments(new ArrayList<>());

        when(commentService.createComment(onboardingRequest.getId(), actionPayload))
                .thenReturn(actionPayload);
        given(commentRepository.getReferenceById(actionPayload.getId())).willReturn(comment);

        var payload = new OnboardingPayload<>(onboardingRequest, actionPayload);

        handler.handle(source, target, payload);

        assertEquals(comment.getId(), onboardingRequest.getComments().get(0).getId());
    }

    @Test
    void guardTest_inReviewAndWithNotaryRole_success() {
        var source = OnboardingState.IN_REVIEW;
        var target = OnboardingState.IN_PROGRESS;
        var actionPayload = an(CommentDTO.class);
        var onboardingRequest = an(OnboardingRequest.class);
        onboardingRequest.setComments(new ArrayList<>());
        var payload = new OnboardingPayload<>(onboardingRequest, actionPayload);

        given(jwtService.hasRole(Roles.NOTARY)).willReturn(true);
        var ret = assertDoesNotThrow(() -> handler.guard(source, target, payload));
        assertTrue(ret);
    }

    @Test
    void guardTest_notInCorrectStatus_returnFalse() {
        var source = OnboardingState.REJECTED;
        var target = OnboardingState.IN_PROGRESS;
        var actionPayload = an(CommentDTO.class);
        var onboardingRequest = an(OnboardingRequest.class);
        onboardingRequest.setComments(new ArrayList<>());
        var payload = new OnboardingPayload<>(onboardingRequest, actionPayload);

        given(jwtService.hasRole(Roles.NOTARY)).willReturn(true);
        var ret = assertDoesNotThrow(() -> handler.guard(source, target, payload));
        assertFalse(ret);
    }

    @Test
    void guardTest_inProgressAndWithApplicantRole_success() {
        var source = OnboardingState.IN_PROGRESS;
        var target = OnboardingState.IN_PROGRESS;
        var actionPayload = an(CommentDTO.class);
        var onboardingRequest = an(OnboardingRequest.class);
        onboardingRequest.setComments(new ArrayList<>());
        var payload = new OnboardingPayload<>(onboardingRequest, actionPayload);

        given(jwtService.hasRole(Roles.APPLICANT)).willReturn(true);
        when(jwtService.getEmail())
                .thenReturn(
                        payload.getOnboardingRequest().getOnboardingApplicant().getEmail());
        var ret = assertDoesNotThrow(() -> handler.guard(source, target, payload));
        assertTrue(ret);
    }

    @Test
    void guardTest_withoutNotaryRole_throwOperationNotPermittedException() {
        var source = OnboardingState.IN_PROGRESS;
        var target = OnboardingState.IN_PROGRESS;
        var actionPayload = an(CommentDTO.class);
        var onboardingRequest = an(OnboardingRequest.class);
        onboardingRequest.setComments(new ArrayList<>());
        var payload = new OnboardingPayload<>(onboardingRequest, actionPayload);

        given(jwtService.hasRole(Roles.NOTARY)).willReturn(false);
        assertThrows(OperationNotPermittedException.class, () -> handler.guard(source, target, payload));
    }
}
