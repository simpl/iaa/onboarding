package eu.europa.ec.simpl.onboarding.statemachine;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

import eu.europa.ec.simpl.common.model.dto.onboarding.OnboardingStatusValue;
import org.junit.jupiter.api.Test;

public class OnboardingStateMapperImplTest {

    @Test
    public void toOnboardingStatusTest_succes() {
        var mapper = new OnboardingStateMapperImpl();
        OnboardingState.values();
        for (OnboardingState onboardingState : OnboardingState.values()) {
            assertDoesNotThrow(() -> mapper.toOnboardingStatus(onboardingState));
        }
    }

    @Test
    public void toOnboardingStateTest_succes() {
        var mapper = new OnboardingStateMapperImpl();
        for (OnboardingStatusValue onboardingStatusValue : OnboardingStatusValue.values()) {
            assertDoesNotThrow(() -> mapper.toOnboardingState(onboardingStatusValue));
        }
    }
}
