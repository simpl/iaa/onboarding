package eu.europa.ec.simpl.onboarding.repositories;

import static eu.europa.ec.simpl.common.test.TestUtil.anOptional;
import static eu.europa.ec.simpl.common.test.TestUtil.anUUID;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.BDDAssertions.catchException;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

import eu.europa.ec.simpl.onboarding.entities.OnboardingRequest;
import eu.europa.ec.simpl.onboarding.exceptions.OnboardingRequestNotFoundException;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class OnboardingRequestRepositoryTest {
    @Spy
    OnboardingRequestRepository onboardingRequestRepository;

    @Test
    void findByIdOrThrow_whenFindByIdReturnsEntity_shouldNotThrowException() {
        given(onboardingRequestRepository.findById(any())).willReturn(anOptional(OnboardingRequest.class));
        var exception = catchException(() -> onboardingRequestRepository.findByIdOrThrow(anUUID()));
        assertThat(exception).isNull();
    }

    @Test
    void findByIdOrThrow_whenFindByIdReturnsEmpty_shouldThrowCertificateNotFoundException() {
        given(onboardingRequestRepository.findById(any())).willReturn(Optional.empty());
        var exception = catchException(() -> onboardingRequestRepository.findByIdOrThrow(anUUID()));
        assertThat(exception).isInstanceOf(OnboardingRequestNotFoundException.class);
    }
}
