package eu.europa.ec.simpl.onboarding.exceptions;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

import org.junit.jupiter.api.Test;

public class RejectionCauseNotProvidedExceptionTest {

    @Test
    public void constructorTest() {
        assertDoesNotThrow(() -> new RejectionCauseNotProvidedException());
    }
}
