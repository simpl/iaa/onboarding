package eu.europa.ec.simpl.onboarding.exceptions;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

import org.junit.jupiter.api.Test;

public class OperationNotPermittedExceptionTest {

    @Test
    public void constructorTest() {
        assertDoesNotThrow(() -> new OperationNotPermittedException(new RuntimeException()));
    }
}
