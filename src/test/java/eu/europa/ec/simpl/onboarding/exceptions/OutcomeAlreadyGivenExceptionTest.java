package eu.europa.ec.simpl.onboarding.exceptions;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

import java.util.UUID;
import org.junit.jupiter.api.Test;

public class OutcomeAlreadyGivenExceptionTest {

    @Test
    public void constructorTest() {
        assertDoesNotThrow(() -> new OutcomeAlreadyGivenException(UUID.randomUUID(), "junit-testing"));
    }
}
