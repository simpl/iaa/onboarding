package eu.europa.ec.simpl.onboarding.services.impl;

import static eu.europa.ec.simpl.common.test.TestUtil.a;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

import eu.europa.ec.simpl.common.model.dto.onboarding.CommentDTO;
import eu.europa.ec.simpl.onboarding.entities.Comment;
import eu.europa.ec.simpl.onboarding.mappers.CommentMapper;
import eu.europa.ec.simpl.onboarding.repositories.CommentRepository;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class CommentServiceImplTest {
    @Mock
    private CommentRepository commentRepository;

    @Mock
    private CommentMapper commentMapper;

    @InjectMocks
    private CommentServiceImpl commentService;

    private UUID onboardingRequestId;
    private CommentDTO commentDTO;
    private Comment commentEntity;

    @BeforeEach
    void setUp() {
        onboardingRequestId = UUID.randomUUID();
        commentDTO = a(CommentDTO.class);
        commentEntity = a(Comment.class).setId(UUID.randomUUID());
    }

    @Test
    void createComment_shouldReturnComment_whenCommentIsCreatedSuccessfully() {
        // Given
        given(commentMapper.toEntity(commentDTO, onboardingRequestId)).willReturn(commentEntity);
        given(commentMapper.toCommentDto(commentEntity)).willReturn(commentDTO);
        given(commentRepository.saveAndFlush(commentEntity)).willReturn(commentEntity);

        // When
        var result = commentService.createComment(onboardingRequestId, commentDTO);

        // Then
        assertThat(result).isEqualTo(commentDTO);
        verify(commentMapper, times(1)).toEntity(commentDTO, onboardingRequestId);
        verify(commentRepository, times(1)).saveAndFlush(commentEntity);
    }

    @Test
    void createComment_shouldThrowException_whenMapperFails() {
        // Given
        given(commentMapper.toEntity(commentDTO, onboardingRequestId))
                .willThrow(new RuntimeException("Mapping failed"));

        // When / Then
        assertThrows(RuntimeException.class, () -> commentService.createComment(onboardingRequestId, commentDTO));
        verify(commentMapper, times(1)).toEntity(commentDTO, onboardingRequestId);
        verify(commentRepository, never()).save(commentEntity);
    }
}
