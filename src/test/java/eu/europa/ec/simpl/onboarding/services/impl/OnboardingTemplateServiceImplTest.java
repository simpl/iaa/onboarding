package eu.europa.ec.simpl.onboarding.services.impl;

import static eu.europa.ec.simpl.common.test.TestUtil.an;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import eu.europa.ec.simpl.common.model.dto.onboarding.DocumentTemplateDTO;
import eu.europa.ec.simpl.common.model.dto.onboarding.OnboardingTemplateDTO;
import eu.europa.ec.simpl.common.model.dto.onboarding.ParticipantTypeDTO;
import eu.europa.ec.simpl.onboarding.entities.DocumentTemplate;
import eu.europa.ec.simpl.onboarding.entities.OnboardingProcedureTemplate;
import eu.europa.ec.simpl.onboarding.entities.ParticipantType;
import eu.europa.ec.simpl.onboarding.exceptions.OnboardingTemplateNotFoundException;
import eu.europa.ec.simpl.onboarding.mappers.ImportMapper;
import eu.europa.ec.simpl.onboarding.repositories.*;
import eu.europa.ec.simpl.onboarding.services.DocumentTemplateService;
import eu.europa.ec.simpl.onboarding.services.ParticipantTypeService;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.instancio.Instancio;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.bean.override.mockito.MockitoBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@Import({OnboardingTemplateServiceImpl.class})
@ImportMapper
class OnboardingTemplateServiceImplTest {
    @MockitoBean
    private OnboardingRequestRepository onboardingRequestRepository;

    @MockitoBean
    private OnboardingStatusRepository onboardingStatusRepository;

    @MockitoBean
    private OnboardingTemplateRepository onboardingTemplateRepository;

    @MockitoBean
    private DocumentRepository documentRepository;

    @MockitoBean
    private ParticipantTypeService participantTypeService;

    @MockitoBean
    private CommentRepository commentRepository;

    @MockitoBean
    private DocumentTemplateService documentTemplateService;

    @MockitoBean
    private DocumentTemplateRepository documentTemplateRepository;

    @MockitoBean
    private MimeTypeRepository mimeTypeRepository;

    @Autowired
    private OnboardingTemplateServiceImpl service;

    @MockitoBean
    private ParticipantTypeRepository participantTypeRepository;

    @Test
    void setOnboardingTemplate_byParticipantType() {
        var doc1 = an(DocumentTemplateDTO.class);
        var doc2 = an(DocumentTemplateDTO.class);

        var participantTypeId = UUID.randomUUID();
        var dto = an(OnboardingTemplateDTO.class);
        dto.setDocumentTemplates(List.of(doc1, doc2));

        var participantType = an(ParticipantTypeDTO.class);
        given(participantTypeService.findParticipantTypeById(participantTypeId)).willReturn(participantType);

        var onboardingTemplate = an(OnboardingProcedureTemplate.class);
        when(onboardingTemplateRepository.findByParticipantTypeId(participantType.getId()))
                .thenReturn(Optional.of(onboardingTemplate));

        var docNew1 = an(DocumentTemplate.class);
        given(documentTemplateService.createDocumentTemplate(doc1)).willReturn(docNew1.getId());
        given(documentTemplateRepository.getReferenceById(doc1.getId())).willReturn(docNew1);
        var docNew2 = an(DocumentTemplate.class);
        given(documentTemplateService.createDocumentTemplate(doc1)).willReturn(docNew2.getId());
        given(documentTemplateRepository.getReferenceById(doc2.getId())).willReturn(docNew2);

        given(onboardingTemplateRepository.save(onboardingTemplate)).willReturn(onboardingTemplate);

        assertDoesNotThrow(() -> service.setOnboardingTemplate(participantTypeId, dto));

        verify(documentTemplateService, times(1)).createDocumentTemplate(doc1);
        verify(documentTemplateService, times(1)).createDocumentTemplate(doc2);
    }

    @Test
    void findOnboardingTemplate_byParticipantType_shouldReturnTemplate() {
        var participantTypeDTO = Instancio.create(ParticipantTypeDTO.class);
        var template = Instancio.create(OnboardingProcedureTemplate.class);

        when(onboardingTemplateRepository.findByParticipantTypeIdOrThrow(participantTypeDTO.getId()))
                .thenReturn(template);

        var result = service.findOnboardingTemplate(participantTypeDTO.getId());

        assertThat(result.getId()).isEqualTo(template.getId());
        verify(onboardingTemplateRepository).findByParticipantTypeIdOrThrow(participantTypeDTO.getId());
    }

    @Test
    void findOnboardingTemplate_byParticipantType_notFound_shouldThrowException() {
        var participantType = "testType";
        var participantTypeDTO = Instancio.create(ParticipantTypeDTO.class);
        var id = participantTypeDTO.getId();

        when(onboardingTemplateRepository.findByParticipantTypeIdOrThrow(id))
                .thenThrow(new OnboardingTemplateNotFoundException(participantType));

        assertThatThrownBy(() -> service.findOnboardingTemplate(id))
                .isInstanceOf(OnboardingTemplateNotFoundException.class);
    }

    @Test
    void deleteOnboardingTemplate_shouldDeleteSuccessfully() {
        var participantTypeDTO = Instancio.create(ParticipantTypeDTO.class);
        var template = Instancio.create(OnboardingProcedureTemplate.class);

        when(onboardingTemplateRepository.findByParticipantTypeIdOrThrow(participantTypeDTO.getId()))
                .thenReturn(template);

        service.deleteOnboardingTemplate(participantTypeDTO.getId());

        verify(onboardingTemplateRepository).delete(template);
    }

    @Test
    void getOnboardingTemplates_success() {

        var pt1 = an(ParticipantType.class);
        var pt2 = an(ParticipantType.class);
        var participantTypes = List.of(pt1, pt2);
        given(participantTypeRepository.findAll()).willReturn(participantTypes);

        var opt1 = an(OnboardingProcedureTemplate.class);
        var opt2 = an(OnboardingProcedureTemplate.class);
        var onboardingProcedureTemplates = List.of(opt1, opt2);
        when(onboardingTemplateRepository.findAllByParticipantTypeIn(participantTypes))
                .thenReturn(onboardingProcedureTemplates);

        var response = assertDoesNotThrow(() -> service.getOnboardingTemplates());
        assertEquals(opt1.getId(), response.get(0).getId());
        assertEquals(opt2.getId(), response.get(1).getId());
    }

    @Test
    void updateOnboardingTemplate_success() {
        UUID participantTypeId = UUID.randomUUID();
        var uuid1 = UUID.randomUUID();
        var uuid2 = UUID.randomUUID();
        var uuids = List.of(uuid1, uuid2);

        var documentTemplate1 = an(DocumentTemplate.class);
        given(documentTemplateRepository.getReferenceById(uuid1)).willReturn(documentTemplate1);
        var documentTemplate2 = an(DocumentTemplate.class);
        given(documentTemplateRepository.getReferenceById(uuid2)).willReturn(documentTemplate2);

        var template = an(OnboardingProcedureTemplate.class);
        when(onboardingTemplateRepository.findByParticipantTypeIdOrThrow(participantTypeId))
                .thenReturn(template);

        assertDoesNotThrow(() -> service.updateOnboardingTemplate(participantTypeId, uuids));
    }
}
