package eu.europa.ec.simpl.onboarding.exceptions;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

import org.junit.jupiter.api.Test;

class InvalidDocumentExceptionTest {

    @Test
    void testConstructor() {
        assertDoesNotThrow(() -> new InvalidDocumentException("Unit test"), "test constructor failed");
    }
}
