package eu.europa.ec.simpl.onboarding.controllers;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

import eu.europa.ec.simpl.common.filters.OnboardingRequestFilter;
import eu.europa.ec.simpl.common.model.dto.onboarding.ApproveDTO;
import eu.europa.ec.simpl.common.model.dto.onboarding.CommentDTO;
import eu.europa.ec.simpl.common.model.dto.onboarding.DocumentDTO;
import eu.europa.ec.simpl.common.model.dto.onboarding.OnboardingRequestDTO;
import eu.europa.ec.simpl.common.model.dto.onboarding.RejectDTO;
import eu.europa.ec.simpl.common.responses.PageResponse;
import eu.europa.ec.simpl.onboarding.services.OnboardingRequestService;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Pageable;

class OnboardingRequestControllerTest {

    @InjectMocks
    private OnboardingRequestController controller;

    @Mock
    private OnboardingRequestService service;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void getOnboardingRequest_shouldReturnRequest_whenFound() {
        // Arrange
        UUID id = UUID.randomUUID();
        OnboardingRequestDTO expectedResponse = mock(OnboardingRequestDTO.class);
        given(service.getOnboardingRequest(id)).willReturn(expectedResponse);

        // Act
        OnboardingRequestDTO actualResponse = controller.getOnboardingRequest(id);

        // Assert
        assertThat(actualResponse).isEqualTo(expectedResponse);
        verify(service).getOnboardingRequest(id);
    }

    @Test
    void create_shouldReturnCreatedRequest() {
        // Arrange
        OnboardingRequestDTO requestDTO = mock(OnboardingRequestDTO.class);
        OnboardingRequestDTO expectedResponse = mock(OnboardingRequestDTO.class);
        given(service.create(requestDTO)).willReturn(expectedResponse);

        // Act
        OnboardingRequestDTO actualResponse = controller.create(requestDTO);

        // Assert
        assertThat(actualResponse).isEqualTo(expectedResponse);
        verify(service).create(requestDTO);
    }

    @Test
    void submit_shouldReturnSubmittedRequest() {
        // Arrange
        UUID id = UUID.randomUUID();
        OnboardingRequestDTO expectedResponse = mock(OnboardingRequestDTO.class);
        given(service.submit(id, "OnboardingRequestController")).willReturn(expectedResponse);

        // Act
        OnboardingRequestDTO actualResponse = controller.submit(id);

        // Assert
        assertThat(actualResponse).isEqualTo(expectedResponse);
        verify(service).submit(id, "OnboardingRequestController");
    }

    @Test
    void requestInformation_shouldReturnUpdatedRequest() {
        // Arrange
        UUID id = UUID.randomUUID();
        OnboardingRequestDTO expectedResponse = mock(OnboardingRequestDTO.class);
        given(service.requestRevision(id, "OnboardingRequestController")).willReturn(expectedResponse);

        // Act
        OnboardingRequestDTO actualResponse = controller.requestRevision(id);

        // Assert
        assertThat(actualResponse).isEqualTo(expectedResponse);
        verify(service).requestRevision(id, "OnboardingRequestController");
    }

    @Test
    void approve_shouldReturnApprovedRequest() {
        // Arrange
        UUID id = UUID.randomUUID();
        ApproveDTO approveDTO = mock(ApproveDTO.class);
        OnboardingRequestDTO expectedResponse = mock(OnboardingRequestDTO.class);
        given(service.approve(id, approveDTO, "OnboardingRequestController")).willReturn(expectedResponse);

        // Act
        OnboardingRequestDTO actualResponse = controller.approve(id, approveDTO);

        // Assert
        assertThat(actualResponse).isEqualTo(expectedResponse);
        verify(service).approve(id, approveDTO, "OnboardingRequestController");
    }

    @Test
    void reject_shouldReturnRejectedRequest() {
        // Arrange
        UUID id = UUID.randomUUID();
        RejectDTO rejectDTO = mock(RejectDTO.class);
        OnboardingRequestDTO expectedResponse = mock(OnboardingRequestDTO.class);
        given(service.reject(id, rejectDTO, "OnboardingRequestController")).willReturn(expectedResponse);

        // Act
        OnboardingRequestDTO actualResponse = controller.reject(id, rejectDTO);

        // Assert
        assertThat(actualResponse).isEqualTo(expectedResponse);
        verify(service).reject(id, rejectDTO, "OnboardingRequestController");
    }

    @Test
    void addComment_shouldReturnUpdatedRequest() {
        // Arrange
        UUID id = UUID.randomUUID();
        CommentDTO commentDTO = mock(CommentDTO.class);
        OnboardingRequestDTO expectedResponse = mock(OnboardingRequestDTO.class);
        given(service.addComment(id, commentDTO, "OnboardingRequestController")).willReturn(expectedResponse);

        // Act
        OnboardingRequestDTO actualResponse = controller.addComment(id, commentDTO);

        // Assert
        assertThat(actualResponse).isEqualTo(expectedResponse);
        verify(service).addComment(id, commentDTO, "OnboardingRequestController");
    }

    @Test
    void search_shouldReturnPagedResults() {
        // Arrange
        OnboardingRequestFilter filter = mock(OnboardingRequestFilter.class);
        Pageable pageable = mock(Pageable.class);
        PageResponse<OnboardingRequestDTO> expectedResponse = mock(PageResponse.class);
        given(service.search(filter, pageable)).willReturn(expectedResponse);

        // Act
        PageResponse<OnboardingRequestDTO> actualResponse = controller.search(filter, pageable);

        // Assert
        assertThat(actualResponse).isEqualTo(expectedResponse);
        verify(service).search(filter, pageable);
    }

    @Test
    void requestAdditionalDocument_shouldReturnUpdatedRequest() {
        // Arrange
        UUID id = UUID.randomUUID();
        DocumentDTO documentDTO = mock(DocumentDTO.class);
        OnboardingRequestDTO expectedResponse = mock(OnboardingRequestDTO.class);
        when(service.requestAdditionalDocument(id, documentDTO, "OnboardingRequestController"))
                .thenReturn(expectedResponse);

        // Act
        OnboardingRequestDTO actualResponse = controller.requestAdditionalDocument(id, documentDTO);

        // Assert
        assertThat(actualResponse).isEqualTo(expectedResponse);
        verify(service).requestAdditionalDocument(id, documentDTO, "OnboardingRequestController");
    }

    @Test
    void uploadDocument_shouldReturnUpdatedRequest() {
        // Arrange
        UUID id = UUID.randomUUID();
        DocumentDTO documentDTO = mock(DocumentDTO.class);
        OnboardingRequestDTO expectedResponse = mock(OnboardingRequestDTO.class);
        when(service.uploadDocument(id, documentDTO, "OnboardingRequestController"))
                .thenReturn(expectedResponse);

        // Act
        OnboardingRequestDTO actualResponse = controller.uploadDocument(id, documentDTO);

        // Assert
        assertThat(actualResponse).isEqualTo(expectedResponse);
        verify(service).uploadDocument(id, documentDTO, "OnboardingRequestController");
    }

    @Test
    void setExpirationTimeframe_shouldCallService() {
        // Arrange
        UUID id = UUID.randomUUID();
        long expirationTimeframe = 3600000L;

        // Act
        controller.setExpirationTimeframe(id, expirationTimeframe);

        // Assert
        verify(service).setExpirationTimeframe(id, expirationTimeframe);
    }

    @Test
    void getDocument_shouldReturnDocument() {
        // Arrange
        UUID requestId = UUID.randomUUID();
        UUID documentId = UUID.randomUUID();
        DocumentDTO expectedResponse = mock(DocumentDTO.class);
        given(service.getDocument(requestId, documentId)).willReturn(expectedResponse);

        // Act
        DocumentDTO actualResponse = controller.getDocument(requestId, documentId);

        // Assert
        assertThat(actualResponse).isEqualTo(expectedResponse);
        verify(service).getDocument(requestId, documentId);
    }

    @Test
    void deleteDocument_shouldCallService() {
        // Arrange
        UUID requestId = UUID.randomUUID();
        UUID documentId = UUID.randomUUID();

        // Act
        controller.deleteDocument(requestId, documentId);

        // Assert
        verify(service).deleteDocument(requestId, documentId);
    }
}
